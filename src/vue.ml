open Js_of_ocaml.Js

class type ['global] config = object
  method globalProperties: 'global prop
end

class type ['data, 'methods, 'computed, 'watch, 'props, 'components, 'directives] create_arg = object
  method data: 'data meth
  method methods: 'methods optdef readonly_prop
  method computed: 'computed optdef readonly_prop
  method watch: 'watch optdef readonly_prop
  method props: 'props optdef readonly_prop
  method template: js_string t optdef readonly_prop
  method render: (Unsafe.any, Unsafe.any) meth_callback optdef readonly_prop
  method emits: js_string t js_array t optdef readonly_prop
  method name: js_string t optdef readonly_prop
  method components: 'components optdef readonly_prop
  method directives: 'directives optdef readonly_prop
end

class type ['data, 'methods, 'computed, 'watch, 'props, 'components, 'directives, 'global] app = object
  method config: 'global config t readonly_prop
  method mount: js_string t -> _ t meth
  method use: Unsafe.any -> _ t meth
  method component: js_string t -> < (_, _, _, _, _, _, _) create_arg; ..> t -> _ t meth
end

class type ['t] prop_arg = object
  method type_: 't constr optdef readonly_prop
  method required: bool t optdef readonly_prop
  method default: 't optdef readonly_prop
  method validator: ('t -> bool t) callback optdef readonly_prop
end

class type lib = object
  method createApp: < ('data, 'methods, 'computed, 'watch, 'props, 'components, 'directives) create_arg; .. > t ->
    < ('data, 'methods, 'computed, 'watch, 'props, 'components, 'directives, _) app; .. > t meth
end

class type ['value] binding = object
  method arg: js_string t optdef readonly_prop
  method value: 'value readonly_prop
  method oldValue: 'value optdef readonly_prop
  method modifiers: Unsafe.any readonly_prop
  method instance: Unsafe.any readonly_prop
  method dir: Unsafe.any readonly_prop
end

let create_app arg =
  let v : lib t = Unsafe.global##._Vue in
  v##createApp (match arg with None -> Unsafe.obj [||] | Some a -> a)

let component ?name (app: < _ app; .. > t as 'a) c : 'a =
  match name, Option.map to_string @@ Optdef.to_option c##.name with
  | Some n, _ | _, Some n -> app##component (string n) c
  | _ -> failwith "no name given for component"

let mount ?(id="app") (app: < _ app; .. > t as 'a) : 'a = app##mount (string ("#" ^ id))
let use (app: < _ app; .. > t as 'a) plugin: 'a = app##use plugin

let set_global (app: < _ app; .. > t) k v =
  Unsafe.set app##.config##.globalProperties (string k) v
