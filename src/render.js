#!/usr/bin/env node
var compiler = require('@vue/compiler-dom')
var render_start = 'return function render(_ctx, _cache) {\n'
var c = compiler.compile(process.argv[process.argv.length-1], {mode:'function', whitespace:'condense', prefixIdentifiers:true})
var i_render_start = c.code.indexOf(render_start)
var j_render_start = i_render_start + render_start.length
var render = c.code.slice(j_render_start).trim()
var before = c.code.slice(0, i_render_start).trim()
console.log('function(_ctx, _cache) {\n  ' + before + '\n' + render)
