all: build
build:
	@dune build src
clean:
	@dune clean
dev:
	@dune build --profile release
	@cp -f _build/default/test/test.bc.js test/test.js

compiler:
	@npm i --prefix src --no-audit --no-fund
	@src/node_modules/esbuild/bin/esbuild --bundle --platform=node --minify --outfile=src/render.bundle.js --log-level=error src/render.js
