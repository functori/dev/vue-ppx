# vue-ppx

`vue-ppx` is a preprocessor to build vue.js applications in `js_of_ocaml`

## Providing arguments

- data
To provide a data argument use:
```ocaml
let%data (message: Js_of_ocaml.Js.js_string Js_of_ocaml.Js.t) = Js_of_ocaml.Js.string "Hello, world!"
```
The constraint on the expression allows to constraint the type when you use this field in methods, computed or other arguments.

- method
In the same way:
```ocaml
let%meth print this (name: Js_of_ocaml.Js.js_string Js_of_ocaml.Js.t) =
  Format.printf "Hi %s!\n%s" (Js_of_ocaml.Js.to_string name) (Js_of_ocaml.Js.to_string this##.message)
```
The first argument `this` is bound to the vue instance and allows to use the data/methods/computed and other.

- computed
```ocaml
let%comp reversed_message this : Js_of_ocaml.Js.js_string Js_of_ocaml.Js.t =
  Js_of_ocaml.Js.string @@
  String.mapi (fun i _c -> String.get s (String.length s - i - 1)) @@
  Js_of_ocaml.Js.to_string this##.message
```
- watch
```ocaml
let%watch message _this new_ old =
  Format.printf "message changed to %s" @@ Js_of_ocaml.Js.to_string new_
```
Constraints are added to ensure new_ and old are of the same type and if the property watched is constrained in you data or props, the type will be constrained in the same way

- props
For components you can give you external props using:
```ocaml
let%prop (prop1 : int) = { dft=3 }
```
In the record given, you can provide the default prop (`default`/`dft`), a validator (`validator`), a javascript constructor (`cons`/`type`) and if the prop is required (`req`/`required`). The constraint will be used in some cases to provide a javascript constructor and set the required flag (if it's not required and no default is provided, use a constraint with `Js_of_ocaml.Js.optdef`). If there is no options in the record, you can use a unit expression.

For data, methods, computed, watch and props, the argument or exit types can be automaticall converted from or to js_of_ocaml using ppx_deriving_jsoo using the `[@conv]` (or `[@@conv]`) attribute.
If the global `conv` option is used (cf. further), a local `[@noconv]` can be used to ignore the conversion for this binding.

- template
For component also the template can be provided using:
```ocaml
{%%template|
<div>
  <input v-model="message" placeholder="message"></input>
  <button @click="print('John')">print</button>
  {{ message }}
</div>
|}
```
If you want the template to be compiled into a render function at the compilation, use:
```ocaml
{%%render|
...
|}
```
If you want to use an external file, use:
```ocaml
{%%file|my_external_file.html|}
```
or
```ocaml
{%%file|$MY_EXTERNAL_FILE_VARIABLE|}
```
Templates can also be compiled into render function when using the environment variable `VUE_COMPILE=1`.
The default compiler is provided in the library (`$OPAM_SWITCH_PREFIX/bin/vue-compiler`) but another one can be provided using `VUE_COMPILER=binary.exe`.

## Instantiating a component

To instantiate a component, use:
```ocaml
[%%comp { components=[ C1; C2.component ]; debug; plugins=[Ezjs_min.Unsafe.global##._MyPlugin]; modules=[Foo, Foo_jsoo]; conv }]
```
It will create a value `component` with the arguments given in the same structure (in the same module). The `components` option allows to register the components `C1.component` and `C2.component` locally for this component. The debug option will print out the expression of this componennt.
- The `plugins` option will set the plugins for the application
- The `conv` option will try to convert all types (for data, props, methods, ...) given to or from js_of_ocaml using ppx_deriving_jsoo.
- The `modules` option makes the correspondance between a module and its counterpart where the convert functions are (for example for the type `Foo.t` when the functions of conversion are `Foo_jsoo.jsoo_conv` as used in ppx_deriving_jsoo.

## Instantiating the application

To instantiate the application, use:
```ocaml
[%%app { components=[ C0 ]; mount="app42"; debug; conv; unhide; export }]
```
It will create a value `_app` with the arguments given in the same structure, with the components registered globally.
- The `mount` option will also mount the application on the element with the id `"app42"` using `Vue.mount ~id:"app42" _app` (the default id is `"app"`).
- The `unhide` option will set the element with id `"app-loading"` to `display=none` and the element with the id `"app"` to `display=block`.
- The `export` option will export the app object to the window (`window.app`).
- `plugins`, `conv` and `modules` can also be used for the root application.

## Using the instance

In method or computed or watch arguments you can use the component instance `$data`, `$props`, `$el`, `$options`, `$parent`, `$root`, `$slots`, `$refs`, `$attrs` using: `[%data]`, `[%props]`, ...
You can also use the instance functions:
- `$emit`:
```ocaml
let%meth f this = [%emit "myevent" this arg1]
```
- `$forceUpdate`: with `[%update]`
- `$nextTick`:
```ocaml
let%meth f this =
  [%next this (fun this -> ...)]
```
